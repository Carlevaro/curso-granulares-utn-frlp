\title[Simulación]{Mecánica de Materiales Granulares}
\subtitle{Simulación de materiales granulares}

\subject{Materiales Granulares}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Resumen}
\large

\begin{itemize}
\item Modelado y simulación (epistemología)
\item Dinámica de colisiones (Event-driven)
\item Elementos discretos (Discrete element method, DEM)
\item Deposición Monte Carlo
\item Deposición balística
\item Pseudo-dinámica
\item Modelos de red
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Modelado y simulación (epistemología)}
  
  \begin{columns}[t]
    \cw{0.3}
    \begin{itemize}
      \item Modelización
      \item Simulación es teoría, no experimento.
    \end{itemize}
    \cw{0.7}
     \centering
     Rol de la simulación

     \includegraphics[width=1.0\textwidth]{figs/modelos.pdf}
  \end{columns}
\normalsize

\vspace{.5cm}
\tiny{\longcite{pugnaloni2008}}
\end{frame}

\begin{frame}
  \frametitle{Dinámica de colisiones (event-driven)}
  \vspace{1em}
\begin{columns}
 \cw{0.7}
 %\small
 \textbf{Algoritmo:}
  \begin{enumerate}
  \item Para posiciones y velocidades dadas de los granos se calcula el instante de la próxima colisión y se avanzan las posiciones sin interacción hasta ese punto.
  \item Se resuelven las ecuaciones de colisión y se asignan nuevas velocidades a las partículas que colisionaron.  
  \item Se regresa al paso (1).
  \end{enumerate}
  
  %\normalsize
  \vspace{0.5cm}
  \textbf{Colapso inelástico}
  En algunas situaciones se pueden producir infinitas colisiones en un tiempo finito debido a la aproximación de ``colisión instantánea''.
  
  Ej: Una partícula con $\varepsilon<1$ rebota sobre el piso. El tiempo de vuelo entre rebotes es $t=2v/g$ si $v$ es la velocidad de rebote. El tiempo necesario para infinitos rebotes es
  
  $$t_{tot} = \sum_{n=0}^{\infty}{\frac{2 \varepsilon^n v_0}{g}}= \frac{2v_0}{g(1-\varepsilon)}$$
 \cw{0.3}
 \begin{center}\ 
 \includegraphics[width=2.5cm]{figs/event-driven.png}\\
 \end{center}
 \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Elementos discretos (discrete element method, DEM)}
  \vspace{1em}

  \begin{columns}
   \cw{0.5}
   \textbf{Newton}
   \small
   $$F(r,\dot{r},t)=m\ddot{r}(t) \rightarrow \left\lbrace \begin{array}{l}
\dot{r}(t)=v(t)\\
\dot{v}(t)=F(r,\dot{r},t)/m
\end{array} \right.  $$

$$r(t+\Delta t)= r(t) + \dot{r}(t) \Delta t + \frac{1}{2} \ddot{r}(t) \Delta t^2 + \frac{1}{3}...$$
$$v(t+\Delta t) = v(t) + \dot{v}(t) \Delta t + \frac{1}{2} \ddot{v}(t) \Delta t^2 + \frac{1}{3}...$$

\textbf{Comentarios}
\begin{itemize}
 \item Si $F$ depende de $v$ se puede agregar un paso (3') donde se estima la velocidad en $t+\Delta t$ como $v(t) + a(t) \Delta t$.
 \item $\Delta t \ll$ duración de cada colisión.
 \item El número de operaciones crece como $N^2$. Usar 3$^{ra}$ ley de Newton, listas de vecinos, celdas enlazadas, etc.
 \item Condiciones de borde.
 \item Es complejo usar partículas no esféricas.
 \item Usar unidades reducidas en el cálculo.
\end{itemize}

   \cw{0.5}
\textbf{Velocity-Verlet}
\small
$$r(t+\Delta t) = r(t) + v(t) \Delta t + \frac{1}{2} a(t) \Delta t^2 \ \ \ \ \text{(1)}$$
$$v(t+\Delta t) = v(t) + \frac{1}{2} [a(t) + a(t+\Delta t)] \Delta t \ \ \text{(2)}$$

En fuerzas conservativas $F(t)$ no depende de $v(t)$.

\begin{enumerate}
 \item Inicializar $r_i$ y $v_i$ $\forall i$.
 \item Calcular $a_i$ midiendo las fuerzas $F_{ij}$.
 \item Actualizar $r_i(t)$ a $r_i(t+\Delta t)$ usando Ec. (1).
 \item Calcular $a_i(t+\Delta t)$ con las nuevas fuerzas.
 \item Actualizar $v_i(t)$ a $v_i(t+\Delta t)$ usando Ec. (2).
 \item Regresar a (3)
\end{enumerate}

\end{columns}
\end{frame}

\begin{frame}
  \frametitle{DEM - Rotaciones}

  \begin{columns}
   \cw{0.5}
   \small
   $$\left(\frac{d\bm{L}}{dt}\right)_s=\bm{\tau}_s$$

   $\bm{L}$ es el momento angular y $\bm{\tau}$ el torque; ambos en el sistema inercial $s$ del CM del cuerpo que no rota con el cuerpo. En el sistema $b$, que sí rota con el cuerpo a velocidad angular $\bm{\omega}$
   
   $$\left(\frac{d\bm{L}}{dt}\right)_s = \left(\frac{d\bm{L}}{dt}\right)_b + \bm{\omega} \times \bm{L}_b$$
   Luego
   $$\left(\frac{d\bm{L}}{dt}\right)_b + \bm{\omega} \times \bm{L}_b= \bm{\tau}_b$$
   
   Si los ejes del sistema $b$ se orientan de modo que el momento de inercia $I$ es diagonal, $L_\alpha = I_\alpha \omega_\alpha$. Reemplazando se llega a las ecuaciones de Euler

      $$I_x\dot{\omega}_x-\omega_y\omega_z (I_y-I_z)=\tau_x^b \ \ \ \text{(1)}$$

   
   \cw{0.5}
   \small
   
   El cambio de orientación del sistema $b$ respecto del sistema que no rota $s$ es
   
   $$\dot{\phi} = -\omega_x^b \frac{\cos \psi}{\sen \theta} + \omega_y^b \frac{\sen \psi}{\sen \theta} \ \ \ \ \text{(2)}$$
   y ecuaciones similares para los otros ángulos de Euler $\psi$ y $\theta$.
   
   \begin{enumerate}
    \item Inicializar $\phi_i$, $\theta_i$ y $\psi_i$ $\forall i$.
    \item Calcular $\bm{\tau}^s$ sobre cada grano.
    \item Transformar $\bm{\tau}^s$ y $\bm{\omega}^s \rightarrow \bm{\tau}^b$ y $\bm{\omega}^b$.
    \item Usar Ecs. (1) y (2) para actualizar $\bm{\omega}^b$,  $\phi_i$, $\theta_i$ y $\psi_i$.
    \item Anti-transformar $\bm{\omega}^b \rightarrow \bm{\omega}^s$
    \item regresar al paso (2).
   \end{enumerate}

   \textbf{Comentario:}
   La Ec. (2) no se puede computar si $\sen \theta =0$. Como alternativa se usa la representación de cuaterniones para las rotaciones.
   \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Deposición Monte Carlo}
\begin{columns}
\column{4cm}  \begin{center}
   \includegraphics[width=3cm]{figs/monte-carlo.png}\\
  \end{center}
\column{8cm}

\begin{enumerate}
 \item Se inicia con posiciones aleatorias.
 \item Se desplaza un grano al azar dentro de una región prescripta.
 \item Si solapa otro grano se lo regresa a su posición original, caso contrario se lo deja en la nueva posición.
 \item Se evalua si se terminó la deposición. Si no es así se regresa al punto (2).
\end{enumerate}

\end{columns}

\textbf{Comentarios:}

\small
\begin{itemize}
 \item Nunca hay contacto entre los granos.
 \item Se puede dejar un pequeño volumen permitido para el movimiento ascendente.
 \item El tamaño de la región prescripta no debe modificarse durante el proceso.
 \item El criterio de finalización puede basarse en la saturación de la densidad o en el porcentaje de movimientos rechazados.
 \item Se pueden simular vibraciones usando expansiones homogéneas o invirtiendo la región prescripta para simular ascenso sistemático de los granos.
 \item Puede simularse flujo por un orificio.
\end{itemize}


\end{frame}

\begin{frame}
  \frametitle{Deposición balística}

  \begin{center}
  \includegraphics[width=10cm]{figs/balistica.png}\\
  
  \end{center}

\vspace{0.2cm}

  \begin{itemize}
 \item Se pueden construir empaquetamientos y pilas.
 \item Se pueden simular vibraciones con expansión homogénea, pero como es determinista tiene un punto fijo de modo que se estanca en una configuración luego de pocos ciclos.
 \item Se pueden simular flujos.
 \item La masa de los granos no es considerada por lo que pueden encontrarse situaciones poco realistas para ser mecánicamente estables.
 \item El modelo considera rodadura perfecta aunque no hay fricción explícita.
 \item No se forman arcos por ser una deposición secuencial. No se observa atasco en orificios.
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Pseudo-dinámica}

\begin{center}
\includegraphics[width=10cm]{figs/pseudo-dinamica.png}\\
\end{center}

\begin{itemize}
 \item Se avanza cada partícula hasta un máximo de $\delta$ en cada paso. 
 \item Se pueden formar arcos y atascos.
 \item Se pueden simular flujos.
 \item La masa de los granos no es tenida en cuenta y se considera rodadura perfecta.
 \item La dinámica se asemeja al movimiento de granos en medios viscosos o en cintas transportadoras.
 \item \textbf{Granos adhesivos:} Se puede incluir una probabilidad de adhesión de modo que un grano queda fijo con sólo un contacto.
 \end{itemize}


\end{frame}

\begin{frame}
  \frametitle{Modelos de red (autómata-celular para pilas y avalanchas)}

  \begin{columns}
\column{4cm} 
\begin{center}
\includegraphics[width=3cm]{figs/avalancha1.png} \\
\vspace{1cm}
\includegraphics[width=3cm]{figs/avalancha2.png} \\
\end{center}

\column{8cm}
\begin{itemize}
 \item $h_i$: altura de la pila en el punto $i$.
 \item $\theta_i=h_i-h_{i+1}$: pendiente de la pila en $i$.
 \item Cuando la pendiente supera un cierto valor se hace que el grano más alto caiga a la siguiente posición: $h_i=h_i-1$ y $h_{i+1}=h_{i+1}+1$.
 \item En tres dimensiones se elije la mayor pendiente de las dos derivadas parciales.
 \item La distribución de tamaños de las avalanchas observadas, $N(s)$, se corresponde con el comportamiento de ley de potencia predicho por la teoría de ``Self-organized criticality'' y algunos experimentos con pilas de arena, $N(s) \propto s^{-3/2}$.  
\end{itemize}
{\tiny \longcite{bak1987}}
\end{columns}

\end{frame}


\begin{frame}
  \frametitle{Modelos de red (autómata-celular para dunas)}

\begin{center}
\includegraphics[width=6cm]{figs/dunas.jpeg} \\
\end{center}
\textbf{Algoritmo:}\footnote{Ver \longcite{poschel2005}, capítulo 6}
\begin{itemize}
    \item Se inicializa con $h_{i,j}$ aleatorio.
 \item Se ``sopla'' $h_{i,j}=h_{i,j}-q$ y  $h_{i+L,j}=h_{i+L,j}+q$
 \item Se ``desliza'' si el gradiente de altura entre $(i,j)$ y un vecino $(i',j')$ excede un umbral $u$: $h_{i,j}=h_{i,j}-mov$ y  $h_{i',j'}=h_{i',j'}+mov$ con $mov < u/2$.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Modelos de red (gas)}

  \begin{columns}
\column{6cm}
\begin{center}
\includegraphics[width=6cm]{figs/gas.jpeg} \\
\end{center}

\column{6cm}
\begin{itemize}
 \item En cada sitio puede haber hasta 7 granos y se asigna un vector de ceros y unos $v=(v_0,... v_6)$.
 \item $v_0=1$: un grano en reposo.
 \item $v_i=1$: un grano moviéndose hacia el vecino $i$
 \item $v_i=0 \ \ \forall i$: no hay granos en el sitio.
\end{itemize}

\begin{center}
\includegraphics[width=4cm]{figs/hexagonal.png} \\
\end{center}

  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{Modelos de red (gas con frustración)}

\begin{center}
\includegraphics[width=3cm]{figs/gas-frustrado.png} \\
\end{center}

\textbf{Reglas:}\footnote{\longcite{nicodemi1997}}
\begin{itemize}
 \item Se exige que siempre $\varepsilon_{ij}S_iS_j=1$ para todos los vecinos $(i,j)$. 
 \item En cada paso se elije al azar un sitio $i$ ocupado y se cambia su estado $S_i$ ($\pm 1$) o se lo migra a un sitio vecino vacío.
 \item Se pueden hacer migraciones asimétricas con una probabilidad $p$ de subir y una probabilidad $1-p$ de bajar.
 \item Se aceptan todos los cambios siempre que cumplan con la restricción $\varepsilon_{ij}S_iS_j=1$. Esto produce frustración de movimientos.
\end{itemize}

\end{frame}

\begin{frame}
 \frametitle{Bibliografía}
 \nocite{poschel2005}
 \printbibliography
\end{frame}

\end{document}
