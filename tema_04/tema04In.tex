\graphicspath{{figs/}}


\title{ Mecánica de materiales granulares } % (optional, use only with long paper titles)
\subtitle{Descripción del estado tensional de empaquetamientos granulares.}

\date[]{}
\author{Manuel Carlevaro}
\institute{Departamento de Ingeniería Mecánica - UTN FRLP \\
          \faEnvelope{} manuel.carlevaro@gmail.com \-  • \- \faTwitter{} @mcarlevaro}

\subject{Materiales Granulares}

\ExecuteBibliographyOptions{sorting=nty} 

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Resumen}
\large

\begin{itemize}
\item Fenomenología
\item Tensor de tensiones de Cauchy (definición y propiedades)
\item Círculo de Mohr
\item Criterio de falla Mohr--Coulomb
\item Efecto Janssen
\item Modelo OSL
\item Otras relaciones constitutivas
\item Promediado: De lo micro a lo macro
\end{itemize}

\end{frame}

%%%
% Ver Granular Physics 
% Anita Mehta
% sección 14.2, página 246 del PDF
%%%
\begin{frame}
  \frametitle{Fenomenología (efecto Janssen)}
\large
    \centering
     \includegraphics[width=7cm]{figs/janssen1.png}

     \begin{itemize}
      \item $F < M g$
      \item La fricción con las paredes cambia la saturación.
      \item La forma de llenado cambia la saturación.
      \item La columna tiene que estar en fricción ``movilizada'' para que se observe la saturación. Esto es, al borde de iniciar el deslizamiento del material sobre las paredes del silo.
     \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Fenomenología (pilas)}
\large
    \centering
     \includegraphics[width=7cm]{figs/pila.png}

     \begin{itemize}
      \item El punto de mayor presión en el fondo no siempre coincide con el centro de la pila.
     \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Fenomenología (función respuesta)}
\large
    \centering
     \includegraphics[width=8cm]{figs/funcion-respuesta.png}

\end{frame}


\begin{frame}
  \frametitle{Distribución de fuerzas de contacto}
\begin{center}
     \includegraphics[width=6cm]{figs/pdf-contactos.png}\hspace{1cm}

$ P(f) \propto \left\{ 
\begin{array}{ll} 
(f/\langle f \rangle)^\alpha & f < \langle f \rangle \\
{\color{red} \exp[-\beta f/\langle f \rangle]} & f > \langle f \rangle \\ 
\end{array} \right. $    
\end{center}

\begin{itemize}
 \item Para $f < \langle f \rangle$ diferentes autores encuentran comportamientos diferentes (meseta, máximo, decrecimiento, etc.). En particular depende de si se miden contactos de la periferia o contactos del centro del sistema.
 \item Estudios recientes muestran que $P(f) \propto \exp[-\beta (f/\langle f \rangle)^2]$ para $f > \langle f \rangle$. 
\end{itemize}

\end{frame}

%%% 
% Ver Continuum Mechanics for Engineers
% G.T. Mase y G.E. Mase
% Capítulo 3
%%%

\begin{frame}
  \frametitle{Tensor de tensión de Cauchy}
  
  \begin{columns}
   \column{3cm}
\begin{center}
     \includegraphics[width=3cm]{figs/cauchy1.png}

     \vspace{1cm}
$ \sigma = F^n/A$

$ \tau = F^t / A$
\end{center}
   \column{9cm}
   
   \begin{itemize}
 \item $\displaystyle{\bm{T}^{(\bm{n})}=\lim_{\Delta s \to 0} \frac{\Delta \bm{F}}{\Delta s} = \frac{d\bm{F}}{d s}}$ existe. $\bm{T}^{(\bm{n})}$ es el \textbf{vector de tensión} (principio de Cauchy).
 \item $\displaystyle{\sigma=\lim_{\Delta s \to 0} \frac{\Delta F^n}{\Delta s}}$ y $\displaystyle{\tau=\lim_{\Delta s \to 0} \frac{\Delta F^t}{\Delta s}}$
 \item $\bm{T}^{(\bm{n})}$ es el mismo para un dado punto del material y un dado $\bm{n}$ sin importar la curvatura de $\Delta s$ (postulado de Cauchy).
 \item $-\bm{T}^{(\bm{n})} = \bm{T}^{(-\bm{n})}$ (lema fundamental de Cauchy). Tercera ley de Newton.
 \item Para cada punto del material existe un tensor $[\sigma]$ tal que el vector de tensión para una normal $\bm{n}$ es $\bm{T}^{(\bm{n})}  = [\sigma] \bm{n} \rightarrow T_j^{(\bm{n})} = \sigma_{ij} n_i$ (teorema de la tensión de Cauchy).
\end{itemize}

  \end{columns}

\end{frame}


\begin{frame}
  \frametitle{Teorema de Cauchy (demostración)}
  
  \begin{columns}
   \column{3cm}
\begin{center}
     \includegraphics[width=3cm]{figs/cauchy2.png}\\
     \vspace{1cm}
     \includegraphics[width=3cm]{figs/cauchy3.png}
\end{center}
   \column{9cm}
   
   Aplicando la segunda ley de Newton sobre el tetraedro de la figura
   
   $$ \bm{T}^{(\bm{n})} dA + \bm{T}^{(-\bm{e}_1)} dA_1 + \bm{T}^{(-\bm{e}_2)} dA_2 + \bm{T}^{(-\bm{e}_3)} dA_3 = dm a$$
   donde $dm$ es la masa del tetraedro y $a$ su aceleración.
   
  Como $\bm{T}^{(-\bm{n})} = -\bm{T}^{(\bm{n})}$ y $dA_1 = (\bm{n} \cdot \bm{e}_1) dA$ etc. tenemos

  $$  \bm{T}^{(\bm{n})} dA - \bm{T}^{(\bm{e}_1)} (\bm{n} \cdot \bm{e}_1) dA - \bm{T}^{(\bm{e}_2)} (\bm{n} \cdot \bm{e}_2) dA - \bm{T}^{(\bm{e}_3)} (\bm{n} \cdot \bm{e}_3) dA = dm a $$
  
  Si $dm \rightarrow 0$, entonces
  
  $$  \bm{T}^{(\bm{n})} =  \bm{T}^{(\bm{e}_1)} n_1 + \bm{T}^{(\bm{e}_2)} n_2 + \bm{T}^{(\bm{e}_3)} n_3 $$
  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{Teorema de Cauchy (demostración)}
  
  \begin{columns}
   \column{3cm}
\begin{center}
     \includegraphics[width=3cm]{figs/cauchy2.png}\\
     \vspace{1cm}
     \includegraphics[width=3cm]{figs/cauchy3.png}
\end{center}
   \column{9cm}
   
     
  $$  \bm{T}^{(\bm{n})} =  \bm{T}^{(\bm{e}_1)} n_1 + \bm{T}^{(\bm{e}_2)} n_2 + \bm{T}^{(\bm{e}_3)} n_3 $$
  
  El vector de tensión en cada cara puede descomponerse en sus componentes cartesianas como
  
  \begin{align}
   \bm{T}^{(\bm{e}_1)} = T_1^{(\bm{e}_1)} \bm{e}_1 +  T_2^{(\bm{e}_1)} \bm{e}_2 + T_3^{(\bm{e}_1)} \bm{e}_3 = \sigma_{11} \bm{e}_1 + \sigma_{12} \bm{e}_2 +\sigma_{13} \bm{e}_3 \notag\\
   \bm{T}^{(\bm{e}_2)} = T_1^{(\bm{e}_2)} \bm{e}_1 +  T_2^{(\bm{e}_2)} \bm{e}_2 + T_3^{(\bm{e}_2)} \bm{e}_3  = \sigma_{21} \bm{e}_1 + \sigma_{22} \bm{e}_2 +\sigma_{23} \bm{e}_3 \notag\\
   \bm{T}^{(\bm{e}_3)} = T_1^{(\bm{e}_3)} \bm{e}_1 +  T_2^{(\bm{e}_3)} \bm{e}_2 + T_3^{(\bm{e}_3)} \bm{e}_3  = \sigma_{31} \bm{e}_1 + \sigma_{32} \bm{e}_2 +\sigma_{33} \bm{e}_3 \notag
  \end{align}

  Luego
  $$  \bm{T}^{(\bm{n})} = \sigma_{ij} \bm{e}_j n_i \rightarrow \bm{T}_j^{(\bm{n})} = \sigma_{ij} n_i \rightarrow \boxed{ \bm{T}^{(\bm{n})} = [\sigma] \bm{n} }$$
  
  Tensión normal y tangencial:
  
  $$\sigma_{\bm{n}} = \bm{T}^{(\bm{n})} \cdot \bm{n} = ([\sigma]\bm{n}) \cdot \bm{n} = \sigma_{ij} n_i n_j$$
  $$\tau_{\bm{n}} = \sqrt{(\bm{T}^{(\bm{n})})^2 - \sigma_{\bm{n}}^2} = \sqrt{\sigma_{ij}\sigma_{ik}n_jn_k - (\sigma_{ij}n_in_j)^2} $$
  \end{columns}

\end{frame}


\begin{frame}
 \frametitle{Equilibrio mecánico y tensor de Cauchy (traslación)}

 Un cuerpo está sometido a fuerzas $ \bm{T}^{(\bm{n})}$ en su superficie y fuerzas $\bm{F}$ en su volumen. Si no posee aceleración las fuerzas aplicados se anulan, luego

 $$\oint_S T_i^{(\bm{n})} dS + \int_V F_i dV = 0 \ \ \ \ \forall i$$

  $$\oint_S \sigma_{ji}n_j dS + \int_V F_i dV = 0 \ \ \ \ \forall i$$

  Pero el teorema de Gauss dice que 

  $$\oint_S \sigma_{ji}n_j dS = \int_V \text{div}(\sigma_{ji}) dV = \int_V \sigma_{ji,j} dV \text{ \ \ \ donde \ \ \  } \text{div}(\sigma_{ji})=\sigma_{ji,j}=\sum_j \frac{\partial \sigma_{ji}}{\partial x_j}$$

  Luego
  
  $$ \int_V \left[ \sigma_{ij,j} + F_i \right] dV = 0 \text{ \ \ entonces para \ \ } V \rightarrow 0 \text{ \ \ tenemos \ \ } \boxed{\sigma_{ij,j} + F_i = 0}$$
\end{frame}

\begin{frame}
 \frametitle{Equilibrio mecánico y tensor de Cauchy (rotación)}

Si el cuerpo anterior además no posee aceleración angular los torques aplicados se anulan, luego

$$\oint_S (\bm{r} \times \bm{T}^{(\bm{n})}) dS + \int_V (\bm{r} \times \bm{F}) dV = \bm{0} $$

La componente $\bm{e}_3$ será

$$\oint_S (x_1 T_2^{(\bm{n})} - x_2 T_1^{(\bm{n})}) dS + \int_V (x_1 F_2 - x_2 F_1) dV = 0 $$

Según la definición del tensor de Cauchy

$$\oint_S (x_1 \sigma_{i2} n_i - x_2 \sigma_{i1} n_i) dS + \int_V (x_1 F_2 - x_2 F_1) dV = 0 $$

Usando el teorema de Gauss

$$\int_V \text{div}(x_1 \sigma_{i2} - x_2 \sigma_{i1}) dV + \int_V (x_1 F_2 - x_2 F_1) dV = 0 $$

\end{frame}

\begin{frame}
 \frametitle{Equilibrio mecánico y tensor de Cauchy (rotación)}


$$\int_V \text{div}(x_1 \sigma_{i2} - x_2 \sigma_{i1}) dV + \int_V (x_1 F_2 - x_2 F_1) dV = 0 $$

$$\int_V (x_{1,i} \sigma_{i2} + x_1 \sigma_{i2,i}- x_{2,i} \sigma_{i1} - x_2 \sigma_{i1,i} + x_1 F_2 - x_2 F_1) dV = 0 $$

$$\int_V [\delta_{1i} \sigma_{i2} - \delta_{2i} \sigma_{i1} + x_1 (\sigma_{i2,i} + F_2) - x_2 (\sigma_{i1,i} +  F_1)] dV = 0 $$

donde $\delta_{ij}$ es la delta de Kronecker y los dos últimos términos se cancelan por la condición de equilibrio de traslación. Luego

$$\int_V (\sigma_{12} - \sigma_{21}) dV = 0 \text{ \ \ \ entonces \ \ \ } \boxed{\sigma_{12} = \sigma_{21}} $$
\end{frame}

\begin{frame}
 \frametitle{Tensor principal de tensión}

 \begin{itemize}
  \item El tensor $[\sigma]$ se puede diagonalizar eligiendo (rotando) el sistema cartesiano de modo de obtener los ejes principales y los autovalores $\sigma_1$, $\sigma_2$ y $\sigma_3$ que forman su diagonal. 
  \item Cuando $[\sigma]$ es simétrico (sistema mecánicamente estable) los autovalores son reales.
  \item Los coeficientes de la ecuación característica son invariantes ya que no dependen del sistema coordenado y son:
  
  $$I_1=Tr([\sigma]); \ \ \ I_2=\sigma_1\sigma_2+\sigma_2\sigma_3+\sigma_3\sigma_1; \ \ \ I_3=\det([\sigma])$$
  
  \item Puede descomponerse $[\sigma]$ en una parte hidrostática $\frac{1}{3}\text{Tr}([\sigma])$ y un deviator $s_{ij}$
  
  $$\sigma_{ij} = \frac{1}{3}\text{Tr}([\sigma])\delta_{ij} + s_{ij}$$
  
 \end{itemize}


 \end{frame}

\begin{frame}
 \frametitle{Círculo de Mohr (2D)}

\begin{columns}
 
 \column{9cm}
En el sistema coordenado principal $(1,2)$ la tensión normal al plano es
 $$ \sigma_n = \sigma_{ij} n_i n_j = \sigma_1 n_1^2 + \sigma_2 n_2^2.$$
 
Luego, $\sigma_n$ puede escribirse como

$$\sigma_n = \sigma_1 \cos^2\theta + \sigma_2 \sen^2\theta =  \sigma_1 \left( \frac{1+\cos2\theta}{2} \right) + \sigma_2  \left(\frac{1-\cos2\theta}{2} \right)$$

$$ \boxed{ \sigma_n = \frac{\sigma_1 + \sigma_2}{2} + \frac{\sigma_1-\sigma_2}{2} \cos2\theta }$$


 \column{3cm}
       \includegraphics[width=3cm]{figs/mohr1.png}\\
En $(x,y)$:
$$ [\sigma] = \left[ \begin{array}{ll} \sigma_{xx} & \sigma_{xy}\\ \sigma_{yx} & \sigma_{yy}   \end{array} \right]  $$

En $(1,2)$, $[\sigma]$ es diagonal:
$$ [\sigma] =  \left[ \begin{array}{ll} \sigma_1 & 0 \\ 0 & \sigma_2   \end{array} \right]  $$

con $\sigma_1 > \sigma_2$
\end{columns}
\end{frame}

\begin{frame}
 \frametitle{Círculo de Mohr (2D)}

\begin{columns}
 
 \column{9cm}
En el sistema coordenado principal $(1,2)$ la tensión tangencial al plano es

$$\tau_n^2 + \sigma_n^2 = \sigma_{ij}\sigma_{ik}n_j n_k = \sigma_1^2 n_1^2 + \sigma_2^2 n_2^2.$$
 
Luego, $\tau_n$ puede escribirse como:

$$\tau_n = \sigma_1^2 \cos^2\theta + \sigma_2^2 \sen^2\theta - [\sigma_1 \cos^2\theta + \sigma_2 \sen^2\theta]^2.  $$

Usando $\cos^2\theta = \frac{1}{2}(1+\cos2\theta)$ y $\cos\theta \sen\theta = \frac{1}{2}\sen2\theta$ obtenemos
$$ \boxed{ \tau_n = \frac{\sigma_1 - \sigma_2}{2} \sen2\theta }$$


 \column{3cm}
       \includegraphics[width=3cm]{figs/mohr1.png}\\
En $(x,y)$:
$$ [\sigma] = \left[ \begin{array}{ll} \sigma_{xx} & \sigma_{xy}\\ \sigma_{yx} & \sigma_{yy}   \end{array} \right]  $$

En $(1,2)$, $[\sigma]$ es diagonal:
$$ [\sigma] =  \left[ \begin{array}{ll} \sigma_1 & 0 \\ 0 & \sigma_2   \end{array} \right]  $$

con $\sigma_1 > \sigma_2$
\end{columns}
\end{frame}

\begin{frame}
 \frametitle{Círculo de Mohr (2D)}
 \vspace{1em}
\begin{columns}
 
 \column{9cm}
Las ecuaciones de $\sigma_n$ y $\tau_n$ son la ecuación paramétrica de un círculo de radio $\frac{\sigma_1-\sigma_2}{2}$ con centro en $(\frac{\sigma_1+\sigma_2}{2}, 0)$

$$ \sigma_n = \frac{\sigma_1 + \sigma_2}{2} + \frac{\sigma_1-\sigma_2}{2} \cos2\theta$$
$$ \tau_n = \frac{\sigma_1 - \sigma_2}{2} \sen2\theta $$
\vspace{0.1cm}
\begin{center}
        \includegraphics[width=5cm]{figs/mohr2.png}
\end{center}

 \column{3cm}
        \includegraphics[width=3cm]{figs/mohr1.png}\\

$$ [\sigma] = \left[ \begin{array}{ll} \sigma_{xx} & \sigma_{xy}\\ \sigma_{yx} & \sigma_{yy}   \end{array} \right]  $$

Ejemplos:
$$\bm{n} = (1, 0); \ \ \ \bm{t}=(0, 1)$$
$$\sigma _n=\sigma_{xx} \ \ \ \ \tau_n=\sigma_{xy}$$
$$\bm{n} = (0, 1); \ \ \bm{t}=(-1, 0)$$
$$\sigma _n=\sigma_{yy} \ \ \ \ \ \tau_n=-\sigma_{xy}$$


\end{columns}
\end{frame}


\begin{frame}
 \frametitle{Criterio de falla de un material granular (Mohr--Coulomb)}
%%%
% Ver Granular Physics 
% Anita Mehta
% sección 14.2.3, página 268 del PDF
%%%

\begin{center}
         \includegraphics[width=9cm]{figs/mohr-coulomb1.png}\\
\end{center}

\begin{itemize}
 \item $\sigma_{yy} = \frac{W}{S} \ \ \ \ \sigma_{xy}=\frac{F}{S}$.
 \item El material cede bajo una tensión tangencial superior al ``shear strength''$=\mu (\sigma_{yy}+\sigma_a)$ (criterio de Coulomb).
 \item Cuando el material no es cohesivo $\sigma_a=0$.
 \item $\mu = \tan\phi$.
 
\end{itemize}

\end{frame}



\begin{frame}
 \frametitle{Criterio de falla de un material granular (Mohr--Coulomb)}

\begin{center}
         \includegraphics[width=5cm]{figs/mohr-coulomb2.png}\\
\end{center}

\begin{itemize}
 \item $\varphi_1 =$ ángulo del plano de deslizamiento respecto del autovector de $\sigma_1$.
 \item $\varphi_1 = (\frac{1}{4} \pi + \frac{1}{2} \phi)$, \ \ \ $\varphi_2 = -(\frac{1}{4} \pi + \frac{1}{2} \phi)$.
 \item Si $\sigma_a=0 \rightarrow \overline{PA} = \overline{PB} = \frac{\sigma_1 - \sigma_2}{2}$, $\overline{QP}=\frac{\sigma_1+\sigma_2}{2}$, $\overline{PA}= \overline{QP} \sen\phi$, Luego 
 
 $$\boxed{\sigma_2 = \frac{1-\sen \phi}{1+\sen \phi} \sigma_1} $$
 En el punto de deslizamiento la relación entre los autovalores es conocida.
\end{itemize}

\end{frame}


\begin{frame}
 \frametitle{Efecto Janssen}
%%%
% Ver Granular Media. Between Fluid and Solid 
% Bruno Andreotti, Yoel Forterre, Olivier Pouliquen 
% Cambridge 2013
% sección 3.4.1, página 96 del PDF
%
%%%
 \vspace{1em}
 \begin{columns}
  \column{8.5cm}
  $\pi R^2\sigma_{zz}(z+dz) - \pi R^2 \sigma_{zz}(z) - \rho g \pi R^2 dz + \sigma_{rz}(R) 2 \pi R dz = 0$
\begin{itemize}
 \item Fricción completamente movilizada: $\sigma_{rz}(R)=\mu_s \sigma_{rr}(R)$.
 \item Asumimos que $\sigma_{rr} = k \sigma_{zz}$ $\forall (z,r)$  (relación constitutiva). $k$ es la ``constante de redirección de fuerzas''.
\end{itemize}
$$ \frac{\partial \sigma_{zz}(z)}{\partial z} - \rho g + \frac{2 \mu_s k}{R} \sigma_{zz}(z) = 0$$
$$ \sigma_{zz}(z=0) = 0$$

Proponemos una solución: $\sigma_{zz}(z)= A \exp[\frac{-2 \mu k z}{R}]+C$. 
Reemplazando se obtiene: $C=\frac{\rho g R}{2 \mu_s k}$ y $A=\frac{-\rho g R}{2\mu_s k}$.

Luego
$$\boxed{ \sigma_{zz}(z)= \frac{\rho g R}{4 \mu_s k} \left[1 - \exp\left(\frac{-4 \mu_s k z}{R}\right)\right] }$$

Experimentalmente se comprueba que la masa aparente de saturación es proporcional a $R^3$ que es consistente con $\sigma_{zz} \rightarrow \frac{\rho g R}{2 \mu_s k}$.

  \column{3.5cm}
  \begin{center}
         \includegraphics[width=3.5cm]{figs/janssen2.png}
\end{center}
 \end{columns}

\end{frame}

\begin{frame}
 \frametitle{Modelo OSL (Oriented Stress Linearity)}
Se puede tomar la condición de equilibrio mecánico ($\bm{\sigma}_{ij,j}+F_i=0$; $\sigma_{ij}=\sigma_{ji}$) y agregar una relación constitutiva para tener un sistema cerrado de ecuaciones. En 2D...

 \begin{columns}
  \column{8.5cm}
  \begin{align}
  &\sigma_{xx} = k \sigma_{zz} \text{ (relación constitutiva)}  \\
  &\frac{\partial \sigma_{xx}}{\partial x} + \frac{\partial \sigma_{zx}}{\partial z} = 0 \\
  &\frac{\partial \sigma_{xz}}{\partial x} + \frac{\partial \sigma_{zz}}{\partial z} = -\rho g 
  \end{align}

Reemplazando (1) en (2) y derivando respecto de $z$ tenemos 
$\frac{\partial^2 \sigma_{xz}}{\partial z^2} = -k \frac{\partial^2 \sigma_{zz}}{\partial z \partial x} $.
Derivando respecto de x la Ec.~(3) tenemos
$ \frac{\partial^2 \sigma_{zz}}{\partial x \partial z} = - \frac{\partial^2 \sigma_{xz}}{\partial x^2} $. De estas ecuaciones podemos obtener una ecuación diferencial separada para $\sigma_{xz}$
$$\frac{\partial^2 \sigma_{xz}}{\partial z^2} = k \frac{\partial^2 \sigma_{xz}}{\partial x^2} $$

Ecuaciones idénticas pueden escribirse para $\sigma_{xx}$ y $\sigma_{zz}$ que corresponden a ecuaciones de ondas con velocidad de propagación $c=\sqrt{k}$.
  \column{3.5cm}
  \begin{center}
         \includegraphics[width=3.5cm]{figs/OSL1.png}\\
         \vspace{0.5cm}
         
         \includegraphics[width=3.5cm]{figs/OSL2.jpg}
\end{center}
 \end{columns}

\end{frame}


\begin{frame}
 \frametitle{Tensor de tensión partiendo de las fuerzas de contacto}
 %%%
 % Ver Granular Physics 
 %  Anita Mehta
 % sección 14.2.2, pg. 253 del PDF 
 %%%
 
Sean $N$ granos con posiciones $\bm{r}_i$ y velocidades $\bm{v}_i$. La densidad a escala microscópica es:
$$ \rho_{mic}(\bm{r},t) = \sum_{i=1}^N {m_i \delta (\bm{r}-\bm{r}_i(t))} \rightarrow \int_V{\rho_{mic}(\bm{r},t)d\bm{r}} = \sum_{i=1}^N{m_i}$$
La cantidad de movimiento por unidad de volumen es:
$$\bm{p}_{mic}(\bm{r},t) = \sum_{i=1}^N{m_i \bm{v}_i(t) \delta(\bm{r}-\bm{r}_i(t))} \rightarrow \int_V{\bm{p}_{mic}(\bm{r},t)d\bm{r}}= \sum_{i=1}^N{m_i\bm{v}_i(t)}$$

\begin{columns}
\column{9cm}
Para pasar al continuo suavizamos con una función localizada $\phi(\bm{R})$ positiva y normalizada de modo de obtener las propiedades en un volumen ``pequeño pero macroscópico''.

$$\rho(\bm{r},t)=\int{\phi(\bm{r}-\bm{r}')\rho_{mic}(\bm{r}',t) = \sum_{i=1}^N{m_i\phi(\bm{r}-\bm{r}_i(t))}} $$

$$\bm{p}(\bm{r},t)=\int{\phi(\bm{r}-\bm{r}')\bm{p}_{mic}(\bm{r}',t) = \sum_{i=1}^N{m_i\bm{v}_i\phi(\bm{r}-\bm{r}_i(t))}} $$

\column{3cm}
         \includegraphics[width=3cm]{figs/phiR.png}
\end{columns}

\end{frame}

\begin{frame}
 \frametitle{Tensor de tensión partiendo de las fuerzas de contacto}

Se puede demostrar que:
$$ \boxed{\frac{\partial\rho(\bm{r},t)}{\partial t} + \nabla \cdot \bm{p}(\bm{r},t) = 0 } \text{ (conservación de la masa)}.$$
Y aplicando la segunda ley de Newton sobre un cubo pequeño de material:
$$\boxed{\frac{\partial p_\alpha(\bm{r},t)}{\partial t} + \frac{\partial \sigma_{\alpha\beta}(\bm{r},t)}{\partial r_\beta} = F_\alpha^{ext}(\bm{r},t)} \text{ (conservación del momento)}.$$
\begin{columns}
\column{9cm}
Aquí $\alpha$ y $\beta$ pueden ser las direcciones cartesianas $x$, $y$ o $z$. $\bm{F}$ es una fuerza externa que actúa sobre todo elemento de volumen (e.g., la gravedad) y $\sigma_{\alpha\beta}$ es el tensor de tensiones en el cubo pequeño.

\column{3cm}
         \includegraphics[width=3cm]{figs/micro-macro1.png}
\end{columns}

El valor de $\frac{\partial p_\alpha(\bm{r},t)}{\partial t}$ se puede poner en función de las variable microscópicas $\bm{r}_i$ y $\bm{v}_i$ de las definiciones previas.
$$ \frac{\partial p_\alpha(\bm{r},t)}{\partial t} = \sum_{i=1}^N{m_i \frac{\partial v_{i\alpha}}{\partial t} \phi(\bm{r}-\bm{r}_i(t))} - \frac{\partial}{\partial r_\beta}\sum_{i=1}^N{m_i v_{i\alpha} v_{i\beta} \phi(\bm{r}-\bm{r}_i(t)) }$$

\end{frame}

\begin{frame}
 \frametitle{Tensor de tensión partiendo de las fuerzas de contacto}

En el primer término: $m_i \frac{\partial v_{i \alpha}}{\partial t} = f_{i\alpha}^{ext} + \sum_{j\neq i}{f_{ij\alpha}}$. Teniendo en cuenta que $f_{ij\alpha} = -f_{ji\alpha}$, puede escribirse
 \begin{align}
 \frac{\partial p_\alpha(\bm{r},t)}{\partial t} =& \frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N{f_{ij\alpha}[\phi(\bm{r}-\bm{r}_i(t)) - \phi(\bm{r}-\bm{r}_j(t))] } \notag \\
 &- \frac{\partial}{\partial r_\beta}\sum_{i=1}^N{m_i v_{i\alpha} v_{i\beta} \phi(\bm{r}-\bm{r}_i(t)) } + F_\alpha^{ext}(\bm{r},t) \notag
 \end{align}

Se puede demostrar que: 
$$ \phi(\bm{r}-\bm{r}_i(t)) - \phi(\bm{r}-\bm{r}_j(t)) = r_{ij\beta}\frac{\partial}{\partial r_\beta}\int_0^1{\phi(\bm{r}-\bm{r}_i(t)+s\bm{r}_{ij}(t))ds}$$

\end{frame}

\begin{frame}
 \frametitle{Tensor de tensión partiendo de las fuerzas de contacto}

Luego, reemplazando en la ecuación de conservación de cantidad de movimiento

\begin{align}
\sigma_{\alpha\beta} =& \frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N{f_{ij\alpha}r_{ij\beta}\int_0^1{\phi(\bm{r}-\bm{r}_i(t)+s\bm{r}_{ij}(t))ds} } \notag \\
&- \sum_{i=1}^N{m_i v_{i\alpha} v_{i\beta} \phi(\bm{r}-\bm{r}_i(t)) } \notag
\end{align}

Si tomamos $\phi(\bm{R})=1/V$ para $R \leq V^{1/3}$ y $\phi(\bm{R})=0$ para $R > V^{1/3}$ obtenemos la fórmula de Born--Huang

\begin{columns}
\column{9cm}
$$\boxed{\sigma_{\alpha\beta} = \frac{1}{2V}\sum_{i=1}^N\sum_{j=1}^N{f_{ij\alpha}b_{ij\beta}} - \frac{1}{V}\sum_{i=1}^N{m_i v_{i\alpha} v_{i\beta}} }$$
Donde sólo se consideran las partículas $i$ dentro del volumen de interés $V$ y $\bm{b}_{ij}$ son los ``branch vector'' de los contactos. $\bm{b}_{ij}$ sólo incluye la porción del vector $\bm{r}_{ij}$  que está dentro del volumen de interés $V$.
\column{3cm}
         \includegraphics[width=3cm]{figs/micro-macro2.png}
\end{columns}

\end{frame}



\section*{Bibliografía}
%\begin{multicols}{2}[\frametitle{\insertsection} \usebeamertemplate{frametitle}]
%\nocite{mase2009}
%\begin{multicols}{2}[\frametitle{Bibliografía} \usebeamertemplate{frametitle}]
%\tiny
%\printbibliography
    %%\bibliography{Biblio.bib}
%%\bibliographystyle{auteurfr}
%\end{multicols}

\begin{frame}{Bibliografía}
\nocite{andreotti2013,mase2009,mehta2009}
\tiny
\printbibliography
\end{frame}

\end{document}
