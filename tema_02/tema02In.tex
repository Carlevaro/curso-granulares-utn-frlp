\graphicspath{{figs/}}

\title{ Mecánica de materiales granulares } % (optional, use only with long paper titles)
\subtitle{Leyes de contacto. Tribología.}

\date[]{}
\author{Manuel Carlevaro}
\institute{Departamento de Ingeniería Mecánica - UTN FRLP \\
          \faEnvelope{} manuel.carlevaro@gmail.com \-  • \- \faTwitter{} @mcarlevaro}

\subject{Materiales Granulares}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Leyes de Coulomb}
\large
(Las primeras dos ``leyes'' se conocen como leyes de Amontons)

\includegraphics[height=1.8cm]{coulomb.png}

\begin{enumerate}
 \item Para iniciar el movimiento de desplazamiento lateral se requiere que $T\ge \mu_s N$. La constante de proporcionalidad $\mu_s$ se conoce como el coeficiente de fricción estática.
 \item El valor de $T$ para iniciar el movimiento no depende del tamaño del área aparente de contacto.
 \item La fuerza necesaria para mantener el cuerpo en movimiento a velocidad constante es $T=\mu_d N$. Esta fuerza no depende de la velocidad impuesta.
\end{enumerate}



\end{frame}

\begin{frame}{Leyes de Coulomb (comentarios)}
\large
\centering
\includegraphics[height=2cm]{coulomb2.png}\hspace{1cm}
\includegraphics[height=2cm]{coulomb3.png}\\
\includegraphics[height=2cm]{coulomb4.png}\hspace{1cm}
\includegraphics[height=2cm]{coulomb5.png}

\begin{itemize}
 \item Euler demostró que $\mu_d \le \mu_s$
 \item Hay dependencia con el área de contacto en algunos casos.
 \item $T$ no es siempre proporcional a $N$ (adhesión).
 \item Hay una dependencia leve con la velocidad relativa.
 \item Hay una dependencia con el tiempo de envejecimiento del contacto.
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Respuesta mecánica de materiales}

  \begin{columns}
\column{8cm}

\begin{itemize}
 \item Estrés $\sigma=F/A$

 \item Deformación $\epsilon=\delta l/l_0$.

 \item Tensión de fluencia $\sigma_c$.

 \item Modulo de Young $E = \sigma / \epsilon$ en el régimen elástico.

 \item Coeficiente de Poisson $\nu = \epsilon_{\perp}/\epsilon{\parallel}$
   \includegraphics[width=1.5cm]{poisson-ratio.png}\\

\vspace{0.5cm}
 \item Módulo de cizalladura $G=\sigma_{xy}/\gamma_{xy}$ con $\gamma_{xy}=\delta x/l$ la deformación de cizalla y $\sigma_{xy}=F/A$ la tensión de cizalla en el régimen elástico.

 \item En el régimen elástico $E = 2 G (1-\nu)$.
\vspace{0.5cm}
 \item Dureza $\sigma_0 = F/A$ en el régimen plástico o de fluencia. $\sigma_0 \approx 3 \sigma_c$.

\end{itemize}

\column{4cm}
\centering
  \includegraphics[width=4cm]{stress-uniaxial.png}\\
  \vspace{0.2cm}
    \includegraphics[width=4cm]{stress-constitutive.png}\\
      \vspace{0.2cm}

  \includegraphics[width=3cm]{stress-shear.png}\\
    \vspace{0.2cm}

  \includegraphics[width=1.2cm]{hardness.png}

  \end{columns}
\end{frame}
   

\begin{frame}
  \frametitle{Contacto de Hertz (esfera -- plano)}

  \begin{columns}
\column{8cm}   

Área de contacto $A=\pi a^2 \approx 2 \pi R d$. Donde $a\approx \sqrt{2Rd}$.

\begin{itemize}
 \item Si la deformación es elástica y suponemos que está toda concentrada en un volumen cercano al contacto de aproximadamente $(2a)^3$, entonces
 
 $$\epsilon\approx d/(2a) \Longrightarrow \sigma \approx E d/(2a).$$
 
 Luego, la fuerza entre cuerpos es
 
 $$F=\sigma A \approx \frac{E d \pi a^2}{2a} \approx \frac{E d \pi}{2} \sqrt{2Rd} = \frac{\pi}{\sqrt{2}}E d^{3/2} R^{1/2}.$$
 
 El cálculo exacto da como resultado $F= \frac{4}{3}E d^{3/2} R^{1/2}.$
 
 \item Si la deformación es plástica
 
 $$\sigma_0=F/A \Longrightarrow F=2 \pi \sigma_0 R d $$
\end{itemize}

 
\column{4cm}  
\includegraphics[width=4cm]{hertz.png}  

 \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Contacto con asperezas (Greenwood - Williamson)\footfullcite[Ver:][Capítulo 7]{popov2017} }
  \centering
  \includegraphics[width=5cm]{asperezas.png}  

\begin{itemize}
 \item Si todas las asperezas son iguales y asumimos que se deforman elásticamente se obtiene que el área de contacto real es proporcional a $F_N^{2/3}$.
 \item Si suponemos que hay una distribución en las alturas de las asperezas entonces el área real de contacto entre cuerpos es aproximadamente proporcional a $F_N$.
 \item Si asumimos que la deformación es plástica en las asperezas entonces el área real de contacto $A \approx F_N/\sigma_0$
\end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Contacto tangencial (esfera--plano)\footfullcite[Ver:][Capítulo 8]{popov2017}}
  \begin{columns}[c]
      \column{5cm}
      \begin{align*}
          p &= p_0 \left( 1 - (r/a)^2 \right)^{1/2} \\
          \tau &= \tau_0 \left( 1 - (r/a)^2 \right)^{-1/2}
      \end{align*}
      \column{5cm}
 \centering
  \includegraphics[width=5cm]{fretting.png}  
  \end{columns}
  
  \begin{itemize}
 \item Cuando se agrega una fuerza tangencial se crean dos regiones: una de deslizamiento ($\tau > \mu p$) y otra de adhesión ($\tau < \mu p$).
 
 \item Fretting: Desgaste por la frotación en la zona de deslizamiento ante cargas tangenciales cíclicas.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Fricción de rodadura\footfullcite[Ver:][Capítulo 9]{popov2017}}
 \centering
  \includegraphics[width=5cm]{rolling.png}  
  
  \begin{itemize}
 \item $F = \mu_r F_N$
 
 \item La velocidad de rotación $v_r$ tiene una componente rígida más una elástica. Como resultado $v_r$ es mayor que la velocidad de traslación de la rueda en la aceleración y menor en el frenado.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Modelos microscópicos\footfullcite[Ver:][Capítulo 10]{popov2017} (Coulomb)}
 \centering
  \includegraphics[width=7cm]{micro-coulomb.png}  
  
  \begin{itemize}
 \item Equilibrio $\Longrightarrow$ $F = F_N \tan \theta \Longrightarrow F_{max}= F_N \tan \theta_{max} \Longrightarrow \mu_{max}=\tan \theta_{max}$
 
 Donde $\theta_{max}$ es el máximo ángulo de la superficie.
 
 \item Si este modelo fuera correcto, ¿por qué el coeficiente de fricción estático $\mu_s$ no depende mucho de la rugosidad de las superficies?
\end{itemize}
  
  
\end{frame}

\begin{frame}
  \frametitle{Modelos microscópicos (Bowden--Tabor)}

  Si asumimos que las asperezas sufren deformación plástica entonces $A \approx F_N / \sigma_0$, donde $A$ es el área real de contacto y $\sigma_0$ es la dureza del material.
  
  Si $\tau_c$ es la tensión de cizalla máxima necesaria para hacer fluir un contacto ($\tau_c=\sigma_{xy}^0$ es la tensión de cizalla en la fluencia).
  
  $$ F_s = F_N \frac{\tau_c}{\sigma_0} \Longrightarrow \mu_s=\frac{\tau_c}{\sigma_0}.$$
  
  Se cumple en general para los materiales que $\tau_c\approx \sigma_c/\sqrt{3} = \sigma_0/3/\sqrt{3} \Longrightarrow \mu_s \approx 0.2$.
  
  Donde $\sigma_c$ es  la tensión para fluencia en tracción.
  
  Esta estimación no permite explicar el rango amplio de valores de $\mu_s$ posibles. 
 
  
\end{frame}

\begin{frame}
  \frametitle{Modelos microscópicos (Prandtl--Tomlinson)\footfullcite[Ver:][Capítulo 11]{popov2017}}
\begin{center}
  \includegraphics[width=8cm]{micro-prandtl.png}  
\end{center}
 
  
$$m \ddot{x}(t) = F - \eta \dot{x}(t) - N \sen [kx(t)]$$  

\vspace{1cm}
$F_s = N$ porque si $N< F$ no hay solución estática.

\end{frame}

\begin{frame}
  \frametitle{Inestabilidad (stick--slip)\footfullcite[Ver:][Capítulo 11.3]{popov2017}}
 \centering
  \includegraphics[width=8cm]{stick-slip.png}  
  
  $$m \ddot{x}(t) + \eta \dot{x}(t) + \frac{\partial U}{\partial x} = C (x_0 -x)$$
  
\end{frame}


%
%Ver Physics-of-Dry-Granular-Media.pdf
%página 285
%


\begin{frame}
  \frametitle{Colisiones}
\begin{center}
  \includegraphics[width=8cm]{colision.png}   
\end{center}

  \begin{itemize}
   \item Conservación del momento lineal y angular en todas las colisiones.
   \item La energía cinética puede no conservarse y dar lugar a colisiones inelásticas.
  \end{itemize}

  Coeficiente de restitución  
  $$\varepsilon = \frac{-(v_1 - v_2)}{(u_1 - u_2)} = \sqrt{\frac{K_{despues}}{K_{antes}}}$$
  
  Solución
  $$ \left\lbrace {\begin{array}{ll}
m_1u_1+m_2u_2 = m_1v_1+m_2v_2 & \text{(conservación momento)}\\
v_1-v_2 = - \varepsilon(u_1-u_2) & \text{(restitución)}
     \end{array}} \right.$$

 \small    
  $$v_1 = \frac{\varepsilon m_2(u_2-u_1)+m_1u_1+m_2u_2}{m_1+m_2}; \;  v_2 = \frac{\varepsilon m_1(u_1-u_2)+m_1u_1+m_2u_2}{m_1+m_2} $$
  
\end{frame}


\begin{frame}
  \frametitle{Colisiones (esferas)}
\begin{center}
  \includegraphics[width=5cm]{colision-esferas.png}   
\end{center}

La velocidad relativa $\bm{g}_{ij}$ en el punto de contacto entre dos esferas de radio $R$ es 
  $$\bm{g}_{ij} = (\bm{v}_i - \bm{\omega}_i \times R \bm{n}_{ij}) - (\bm{v}_j - \bm{\omega}_j \times R \bm{n}_{ij}) = \bm{v}_{ij} - R(\bm{\omega}_i + \bm{\omega}_j) \times \bm{n}_{ij}$$
  donde
  $$ \bm{v}_{ij} = \bm{v}_i - \bm{v}_j; \ \ \ \ \ \ \ \ \bm{n}_{ij} = \frac{\bm{r}_i - \bm{r}_j}{|\bm{r}_i - \bm{r}_j|} $$
  y las componentes normales y tangenciales de $\bm{g}_{ij}$ al punto de contacto son
  $$ \bm{g}^n_{ij} = (\bm{g}_{ij} \cdot \bm{n}_{ij}) \bm{n}_{ij}  $$
  $$ \bm{g}^t_{ij} = - \bm{n}_{ij} \times (\bm{n}_{ij} \times \bm{g}_{ij}) =  \bm{g}_{ij} - \bm{g}^n_{ij} $$
\end{frame}

\begin{frame}
  \frametitle{Colisiones (esferas)}

Los coeficientes de restitución normal y tangencial se definen como \\ \footnotesize (primas significan después de la colisión)\normalsize

  $$(\bm{g}^n_{ij})' = - \varepsilon^n \bm{g}^n_{ij} \ \ \ \ \ \ \ 0\leq \varepsilon^n \leq 1$$
  $$(\bm{g}^t_{ij})' = \varepsilon^t \bm{g}^n_{ij} \ \ \ \ \ \ \ -1\leq \varepsilon^t \leq 1$$

  y la conservación del momento lineal y angular es (para masas $m$ y momento de inercia $I$ iguales)
  
  $$ \bm{v}'_{i} + \bm{v}'_{j} = \bm{v}_{i} + \bm{v}_{j} $$
  
  $$ m R (\bm{n}_{ij} \times \bm{v}'_{i} + I \bm{\omega}'_i) = m R (\bm{n}_{ij} \times \bm{v}_{i} + I \bm{\omega}_i) $$
  
  $$ m R (\bm{n}_{ij} \times \bm{v}'_{j} + I \bm{\omega}'_j) = m R (\bm{n}_{ij} \times \bm{v}_{j} + I \bm{\omega}_j) $$
  
  Las dos últimas ecuaciones son la conservación del momento angular para cada partícula alrededor del punto de contacto (donde no hay torque y esta separación es válida).
\end{frame}

\begin{frame}
  \frametitle{Colisiones (esferas)}

La solución, tomando $\overline{I}\equiv I/(m R^2)$ como el momento de inercia reducido\\ \footnotesize (ver T. Poeschel, Computational Granular Dynamics)\normalsize
  $$\bm{v}'_{i} = \bm{v}_{i} - \frac{1+\varepsilon^n}{2} \bm{g}^n_{ij} + \frac{\overline{I}(\varepsilon^t-1)}{2(\overline{I}+1)} \bm{g}^t_{ij}$$
  $$\bm{v}'_{j} = \bm{v}_{j} + \frac{1+\varepsilon^n}{2} \bm{g}^n_{ij} - \frac{\overline{I}(\varepsilon^t-1)}{2(\overline{I}+1)} \bm{g}^t_{ij}$$
  $$\bm{\omega}'_i = \bm{\omega}_i - \frac{\varepsilon^t - 1}{2 R (\overline{I}+1)} (\bm{n}_{ij} \times \bm{g}^t_{ij})$$
  $$\bm{\omega}'_j = \bm{\omega}_j - \frac{\varepsilon^t - 1}{2 R (\overline{I}+1)} (\bm{n}_{ij} \times \bm{g}^t_{ij})$$

\begin{itemize}
  \item Las ecuaciones de conservación de momento angular debieran escribirse para el momento total. La aproximación de separarlas usando como referencia el punto de contacto es sólo válida si el contacto es puntual y no hay torque aplicados en ese punto.
  \item Las expresiones para $\varepsilon^n$ y $\varepsilon^t$ constituyen una ecuación vectorial donde la componente de $\bm{g}_{ij}$ normal al plano definido por $\bm{g}^n_{ij}$ y $\bm{g}^t_{ij}$ es nula. Sin embargo las dos ecuaciones son independientes y completan el sistema de 4 ecuaciones y 4 incógnitas. 
\end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Valores de $\varepsilon^n$}
\begin{center}
  \includegraphics[width=4cm]{restitucion.png}   
\end{center}

\footnotesize
\begin{itemize}
  \item $\varepsilon^n$ depende de la velocidad de colisión normal $\bm{g}^n_{ij}$.
  \item Si se conoce la fuerza de interacción entre las partículas $F(\xi,\ddot{\xi})$ como función de la penetración $\xi$ y su derivada, se puede calcular $\varepsilon^n$ resolviendo a ecuación de Newton
  $$m_{e} \ddot{\xi}(t) = F(\xi(t),\ddot{\xi}(t)), \ \ \ \ \dot{\xi}(0)=|\bm{g}^n_{ij}|, \xi(0)=0$$
  \item Si se conoce la fricción por rodadura $\mu_{r}$ se puede calcular $\varepsilon^n$ usando (ver Brilliantov, EPJB (1999))
  $$\frac{1-\varepsilon^n}{2.28(\rho/m)^{2/5} |\bm{g}^n_{ij}|^{1/5} } = \frac{\mu_r}{V} $$
  donde $V$ es la velocidad lineal de rodadura y 
  $$\rho=\frac{2Y}{3(1-\nu^2)}\frac{\sqrt{R^e}}{m_e}$$
  con $Y$ el módulo de Young y $\nu$ el coeficiente de Poisson.
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Valores de $\varepsilon^t$}
\begin{center}
  \includegraphics[width=5.5cm]{restitucion-tangencial.png}\hspace{0.2cm}\includegraphics[width=2cm]{inversion-rotacion.png}   
  \includegraphics[width=3cm]{cono-coulomb.png}   
   
\end{center}

\footnotesize
\begin{itemize}
  \item Si $\varepsilon^t>0 \longrightarrow$ deslizamiento; si $\varepsilon^t<0 \longrightarrow$ inversión de rotación.
  \item Si $\bm{g}^n_{ij} \rightarrow 0$ la fuerza normal es pequeña y la tangencial también por lo que $\varepsilon^t \rightarrow 1$.
  \item Si $\bm{g}^n_{ij}$ crece la normal y la tangencial crecen por lo que si $\bm{g}^t_{ij}$ es pequeña no habrá deslizamiento sino rodadura perfecta. Si hay inversión de rotación  $\varepsilon^t<0$ el valor de $\varepsilon^t$ no puede llegar a $-1$ porque parte de la energía se pierde en la deformación normal.
  \item Si aplicamos el criterio de Coulomb $F^t_{max}=\mu_s F^n$ entonces la rodadura se produce si $F^t<\mu_s F^n$ y el deslizamiento si $F^t>\mu_s F^n$. Se puede representar un ``cono de Coulomb'' en el contacto con ángulo $\theta = \tg^{-1}(\mu_s)$. Si $F \in $ al cono habrá rodadura, si no habrá deslizamiento.
  \item Dos esferas nunca pueden tener $\varepsilon^t=1$ para todo $\bm{g}^n_{ij}$ aún siendo perfectamente suaves. Cualquier velocidad tangencial con $\bm{g}^n_{ij}>0$ producirá un cambio de velocidad normal que será amortiguada en forma viscosa y alterará $\bm{g}^t_{ij}$.
  \item En ciertas condiciones con fuerte acoplamiento entre rotación y traslación es posible encontrar $\varepsilon^n>1$.
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Modelos de fuerzas para simulación (normales)}
  % Ver Cap. 2 del libro de Poschel
\begin{itemize}
 \item \textbf{Hertz:} Fuerza normal basado en la deformación de esferas elásticas sin roce.
 $$ F^n = \frac{2 Y \sqrt{R^e}}{3(1-\nu^2)} \xi^{3/2}, \ \ \ \ \frac{1}{R^e}=\frac{1}{R_1}+\frac{1}{R_2}, \ \ \ \ \xi= \text{deformación normal}$$
 \item \textbf{Lineal:} 
 $$F^n = Y \xi$$
 \item \textbf{Walton--Braun:} Deformación plástica permanente $\xi_0$
 $$F^n = \left\lbrace \begin{array}{ll}
Y_i \xi & \text{si } \dot{\xi}\geq 0\\
Y_u (\xi-\xi_0) & \text{si } \dot{\xi}< 0  \end{array} \right.$$
 \item \textbf{Spring--dashpot:}
 $$F^n = Y \xi + \gamma^n \dot{\xi}$$
 \footnotesize Esta fuerza lleva a una coeficiente de restitución normal
 $$\varepsilon^n=\exp \left[ \frac{-\pi \gamma^n}{2 m^e} \left( \frac{Y}{m^e} - \left(\frac{\gamma^n}{2 m^e}\right)^2 \right)^{-1/2} \right] $$
 que claramente no depende de la velocidad de la colisión $\dot{\xi}$.
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Modelos de fuerzas para simulación (normales)}
\begin{itemize}
 \item \textbf{Hertz--Kuwabara--Kono:} (ver también Billiantov, Phys. Rev. E (1996))
 $$ F^n = \frac{2 Y \sqrt{R^e}}{3(1-\nu^2)} (\xi^{3/2} + A \sqrt{\xi} \dot{\xi})$$
 donde $A$ depende de la viscosidad del material de las esferas.
 
 Esta fuerza lleva a un coeficiente de restitución normal más realista
 $$\varepsilon^n = 1 + C_1 A \rho^{2/3} {g^n_{ij}}^{1/3} + C_2 A^2 \rho^{4/3} {g^n_{ij}}^{2/3} + \dots$$
 donde $g^n_{ij}$ es la velocidad normal de colisión y $\rho = \frac{2Y}{3(1-\nu^2)}\frac{\sqrt{R^e}}{m_e}$; $C_1=-1.15344$; $C_2=0.79826$; $C_3 = -0.48358$; $C_4=0.28528$; ...
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Modelos de fuerzas para simulación (normales)}
\centering
  Duración de una colisión
  
    \includegraphics[width=9cm]{poeschel-contacto.png}   
    % pg 30 del PDF del libro de Poschel


\end{frame}

\begin{frame}
  \frametitle{Modelos de fuerzas para simulación (tangenciales)}

  \begin{itemize}
   \item \textbf{Haff--Werner:}
   $$F^t = -\text{sign}(\bf{v}^t_{ij}) \text{min}[\gamma^t|\bf{v}^t_{ij}|; \mu |\bf{F}^n|]$$
Este modelo no representa bien el caso de partículas que están en reposo en el contacto ya que $F^t=0$ en ese caso y no es capaz de considerar así la fricción estática.

  \item \textbf{Brilliantov:}
\begin{center}
     \includegraphics[width=4cm]{brilliantov.png}
\end{center}

 
     $$F^t = -\mu F^n \left[ \frac{\zeta}{\zeta_0}- \text{int}\left(\frac{\zeta}{\zeta_0}\right) \right]$$
    \end{itemize}
  donde $\zeta_0$ es la longitud típica de una aspereza y debe ser mucho menor a la deformación tangencial total típica durante una colisión.
\end{frame}

\begin{frame}
  \frametitle{Modelos de fuerzas para simulación (tangenciales)}
  
  \begin{columns}
  \column{8cm}   
  \begin{itemize}
   
  \item \textbf{Cundall--Strack:}
   $$F^t = -\text{sign}(\bf{v}^t_{ij}) \text{min}[k^t \zeta|; \mu |\bf{F}^n|]$$  
  donde $\zeta(t)$ es la deformación tangencial desde el inicio del contacto
  $$\zeta(t)= \int_{t_0}^t{\bf{v}^t_{ij}(t')dt'}$$
    \end{itemize}

    -- Este modelo da resultados razonables si $k^t/Y \approx 2/7$, donde $Y$ es el coeficiente del resorte lineal para la parte normal en un modelos Spring--dashpot.
    
    -- Notar que el resultado del efecto tangencial depende fundamentalmente de la fase en que termina la oscilación cuando termina el contacto.  Esto es, depende de $T^t/T^n$ (cociente del período de oscilación tangencial y normal) que es proporcional a $k^t/Y$.
 \column{4cm}
  \begin{center}
   \includegraphics[width=4cm]{cundall.png}
  \end{center}
  $$\psi_i = \frac{v_i^t}{v_i^n}$$
  $$\psi_f = \frac{v_f^t}{v_i^n}$$
  $$\varepsilon^t = \frac{\psi_f}{\psi_i}$$
  \end{columns}

\end{frame}

\begin{frame}{Bibliografía}
    \nocite{duran2000,andreotti2013}
    \printbibliography 
%     \centering
%     \noindent\rule{4cm}{0.4pt}
%     \bigskip 

     
\end{frame}


\end{document}

