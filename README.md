# Curso Granulares UTN-FRLP

Material de las clases del curso **"Mecánica de Materiales Granulares"** en la Facultad Regional La Plata de la Universidad Tecnológica Nacional.

Para cada tema, el Makefile correspondiente en cada subdirectorio genera dos PDFs, uno con una presentación y otro con el handout correspondiente.

## Modificación de archivos
Para modificar el LaTeX de cada tema hay que:

1. Ir al directorio del tema correspondiente (n):

    `cd tema_0n`

2. Editar el archivo tema0nIn.tex 

3. Compilar con XeLaTex o LuaLaTex según el caso:

    `make`

Con esta instrucción se generan los archivos tema0n-p.pdf y tema0n-ho.pdf.

4. Limpiar los archivos y directorios auxiliares

    `make clean`

5. Actualizar el repositorio:
    ```
    git commit -m "texto descriptivo de la modificación" tema0nIn.tex *pdf
    git push 
    ```

Si se incluyen nuevas figuras, o se modifica una existente, hay que 
actualizarlas también en el repositorio (`git add` la incorpora al repo).

